#coding:utf-8
import uuid
import requests
import Util.RedisUtil
import wxMsg.weChatDataProcessing as wc
import dao.user as userDao

redis = Util.RedisUtil.RedisUtilClass()


def getQrcode():
    url = "https://api.weixin.qq.com/cgi-bin/qrcode/create"
    userToken = str(uuid.uuid4())
    token = redis.getByName("token")
    params = {
        'access_token': token
    }
    data = {
        "expire_seconds": 30,
        "action_name":  "QR_STR_SCENE",
        "action_info": {
            "scene": {
                "scene_str": "login#"+userToken
            }
        }
    }
    r = requests.post(url, params=params, json=data).json()
    result = {
        'userToken': userToken,
        'img': 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket='+r['ticket']
    }
    userinfo = {
        "code": '0'
    }
    redis.setByEx(userToken, str(userinfo), ex=30)
    return result

def login(json):
    userOperId = json['FromUserName']
    user = userDao.query_user_info(userOperId)
    print("----------------------------------")
    print(user)
    if len(user)==0:
        wc.save_logIn_user(json)
    else :
        wc.updeta_user_info(json)
    newUSer = userDao.query_user_info_first(userOperId)
    print("查询之后的数据")
    print(newUSer)
    EventKey = json['EventKey']
    if None==EventKey:
        return
    token = EventKey.split("#")[1]
    userinfo = {
            "code": '1',
            'info':newUSer
    }
    redis.setByEx(token, str(userinfo), ex=60*60*2)


def deleteUser(json):
    userOperId = json['FromUserName']
    userDao.deleteUserByOperId({"wxOperId":userOperId})





