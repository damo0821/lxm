# 微信数据处理
import wxMsg.sendWxTemplate as wx

import service.UserService as userService
from orm.User import User
from Util.MysqlOrm import MysqlOrm
from Util.DateUtils import getLocalTime
import dao.user as userDao

MysqlOrm = MysqlOrm()
session = MysqlOrm.getSession()


def save_logIn_user(json):
    openId = json['FromUserName']
    userOperId = json['FromUserName']
    user = userDao.query_user_info(userOperId)
    if len(user)!=0:
        return
    # 获取用户信息
    user = wx.getUserInfoByOpenId(openId).json()
    subscribe = user['subscribe']
    if '0' == subscribe:
        return ""
    authCode = userService.getAuthCode()
    adduser = User(
        userName=user['nickname'],
        wxOperId=user['openid'],
        passWord='12345678',
        authCode=authCode,
        createTime=getLocalTime(),
        headUrl=user['headimgurl'],
        userType='1',
        maxSenNum=1000,
        currentSenNum=0,
        oneDayNum=0,
        todayMaxNum=0,
        userState='0'
    )
    session.add(adduser)
    session.commit()
    return user





def updeta_user_info(json):
    openId = json['FromUserName']
    # 获取用户信息
    user = wx.getUserInfoByOpenId(openId).json()
    subscribe = user['subscribe']
    if '0' == subscribe:
        return ""
    adduser={
       "userName":user['nickname'],
        "wxOperId":user['openid'],
        "headUrl":user['headimgurl']
    }
    session.query(User).filter(User.wxOperId == openId).update(adduser)
    session.commit()



