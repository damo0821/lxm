# coding:utf-8
import configparser


class configClass:

    def __init__(self):
        self.conf = configparser.ConfigParser()
        self.conf.read('config.ini', encoding='utf-8')

    def getCof(self):
        return self.conf

    def getItem(self, k):
        result = {}
        y = self.conf.items(k)
        for v in y:
            result[v[0]] = v[1]
        return result

