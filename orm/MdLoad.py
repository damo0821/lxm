#coding:utf-8

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, BigInteger, Date

Base = declarative_base()  # 生成orm基类

class MdLoad(Base):
    __tablename__ = 'mdLoad'
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    content = Column(String(1000), nullable=True, index=False)

    def getDict(self):
        user =dict(self.__dict__).copy()
        user['_sa_instance_state'] = ''

        return user
