# coding:utf-8
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, BigInteger, Date

Base = declarative_base()  # 生成orm基类


class User(Base):
    __tablename__ = 'User'
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    userName = Column(String(64), nullable=True, index=False)
    passWord = Column(String(64), nullable=True, index=False)
    wxOperId = Column(String(64), nullable=True, index=False)
    authCode = Column(String(64), nullable=True, index=False)
    phon = Column(String(64), nullable=True, index=False)
    email = Column(String(64), nullable=True, index=False)
    publicName = Column(String(64), nullable=True, index=False)
    userType = Column(String(64), nullable=True, index=False)
    headUrl = Column(String(64), nullable=True, index=False)
    createTime = Column(Date, nullable=True, index=False)
    upDateTime = Column(Date, nullable=True, index=False)
    maxSenNum = Column(Integer, nullable=True, index=False)
    oneDayNum = Column(Integer, nullable=True, index=False)
    currentSenNum = Column(Integer, nullable=True, index=False)
    todayMaxNum = Column(Integer, nullable=True, index=False)
    userState = Column(String(64), nullable=True, index=False)


    def getDict(self):
        user = dict(self.__dict__).copy()
        print(user)
        user['_sa_instance_state'] = ''
        if self.createTime is not None:
            user['createTime'] = self.createTime.strftime('%Y-%m-%d %H:%M:%S')
        if self.upDateTime is not None:
            user['upDateTime'] = self.upDateTime.strftime('%Y-%m-%d %H:%M:%S')
        print(type(self.createTime))
        print(self.createTime)
        for key in user:
            if (user[key] is None):
                user[key] = ''
        return user
