#coding:utf-8

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, BigInteger, Date

Base = declarative_base()  # 生成orm基类

class SmsSenMsg(Base):
    __tablename__ = 'sms_sen_msg'
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    senContent = Column(String(1000), nullable=True, index=False)
    userId = Column(BigInteger, nullable=True, index=False)
    title = Column(String(1000), nullable=True, index=False)
    authCode = Column(String(1000), nullable=True, index=False)
    createTime = Column(Date, nullable=True, index=False)
    senTime = Column(Date, nullable=True, index=False)

    def getDict(self):
        user = dict(self.__dict__).copy()
        user['_sa_instance_state'] = ''
        if self.createTime is not None:
            user['createTime'] = self.createTime.strftime('%Y-%m-%d %H:%M:%S')
        if self.senTime is not None:
            user['senTime'] = self.senTime.strftime('%Y-%m-%d %H:%M:%S')
        print(type(self.createTime))
        print(self.createTime)
        for key in user:
            if (user[key] is None):
                user[key] = ''
        return user
