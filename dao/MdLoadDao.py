# coding:utf-8

from Util.MysqlOrm import MysqlOrm
from Util.DateUtils import getLocalTime
from orm.MdLoad import MdLoad
from sqlalchemy import and_
from orm.SmsSenMsg import SmsSenMsg

MysqlOrm = MysqlOrm()
session = MysqlOrm.getSession()


def update(data):
    adduser = {
        "content": data['content']
    }
    session.query(MdLoad).filter(MdLoad.id == data['id']).update(adduser)
    session.commit()


def query_id(id):
    user = session.query(MdLoad).filter((MdLoad.id == id)).first()
    session.close()
    if (None == user): return ""
    return user.getDict()
