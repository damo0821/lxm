# coding:utf-8
from Util.MysqlOrm import MysqlOrm
from orm.User import User
from Util.DateUtils import getLocalTime

MysqlOrm = MysqlOrm()
session = MysqlOrm.getSession()


def queryUserByAuth(authCode):
    user = session.query(User).filter(User.authCode == authCode).first()
    session.close()

    if (user is None):
        return None
    return user.getDict()


def query_user_info(operId):
    data = session.query(User).filter(User.wxOperId == operId).all()
    session.close()
    return data


def query_user_info_first(operId):
    user = session.query(User).filter(User.wxOperId == operId).first()
    session.close()
    return user.getDict()


def updateUserAuthCodeById(user):
    adduser = {
        "authCode": user['authCode']
    }
    session.query(User).filter(User.id == user['id']).update(adduser)
    session.commit()


def deleteUserByOperId(user):
    session.query(User).filter(User.wxOperId == user['wxOperId']).delete()
    session.commit()


def UpdateUserById(user):
    updateUser = {}
    if 'currentSenNum' in user:
        updateUser['currentSenNum'] = user['currentSenNum']
    if 'oneDayNum' in user:
        updateUser['oneDayNum'] = user['oneDayNum']

    updateUser['upDateTime'] = getLocalTime()
    session.query(User).filter(User.id == user['id']).update(updateUser)
    session.commit()
