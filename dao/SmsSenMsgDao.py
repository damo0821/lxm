# coding:utf-8

from Util.MysqlOrm import MysqlOrm
from orm.SmsSenMsg import SmsSenMsg
from Util.DateUtils import getLocalTime
from sqlalchemy import and_

MysqlOrm = MysqlOrm()
session = MysqlOrm.getSession()


def save(data):
    msg = SmsSenMsg(
        senContent=data['content'],
        authCode=data['authCode'],
        userId=data['userId'],
        title=data['title'],
        createTime=getLocalTime(),
        senTime=getLocalTime()
    )
    session.add(msg)
    session.commit()
    return msg


def queryDataByAuthCOdeAndId(data):
    user = session.query(SmsSenMsg).filter(
        and_(SmsSenMsg.id == data['id'], SmsSenMsg.authCode == data['authCode'])).first()
    session.close()

    if(None == user):return ""
    return user.getDict()


