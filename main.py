# coding:utf-8
import appMain
import action.WxAction
import action.MdAction
import job.getWxToken as job
import ConfigUtil
scheduler = appMain.scheduler

conf = ConfigUtil.configClass()



if __name__ == '__main__':
    flaskConfig = conf.getItem('flask')
    # scheduler.add_job(func=job.printHello, id='1', trigger='cron', second='0/5')
    # scheduler.add_job(func=wx.senWxTemplate, id='2', trigger='cron', second='0/5')
    job.getWxTokenFunction()
    scheduler.add_job(func=job.getWxTokenFunction, id='1', trigger='cron', minute='0/58')
    appMain.scheduler.init_app(appMain.app)  # 把任务列表载入实例flask
    appMain.scheduler.start()  # 启动任务计划
    appMain.app.run(host=flaskConfig['host'], port=flaskConfig['port'], debug=flaskConfig['debug'])
