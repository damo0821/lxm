#coding:utf-8
import uuid
import dao.user as userDao


def updateUserAuthCode(user):
    authCode = getAuthCode()
    user['authCode'] = authCode
    userDao.updateUserAuthCodeById(user)
    return user



def getAuthCode():
        authCode = "WXX-" + str(uuid.uuid4()).replace('-', '')
        return authCode


