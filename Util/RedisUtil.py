# coding:utf-8
import redis
import appMain
import ConfigUtil

config = ConfigUtil.configClass()


class RedisUtilClass:

    def __init__(self):
        redisConfig = config.getItem('redis')
        pool = redis.ConnectionPool(host=str(redisConfig['redis_host']),
                                    password=str(redisConfig['redis_pass_word']),
                                    port=redisConfig['redis_port'],
                                    max_connections=int(redisConfig['redis_max_connections']))
        self.conn = redis.Redis(connection_pool=pool, decode_responses=True)

    def set(self, k, v):
        self.conn.set(k, v)

    def setByEx(self, k, v, ex):
        self.conn.set(k, v, ex=ex)

    def delete_key(self, k):
        self.conn.delete(k)

    def getByName(self, k):
        return self.conn.get(k)
