# coding:utf-8
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from ConfigUtil import configClass

configClass=configClass()
class MysqlOrm:
    def __init__(self):
        ormMysql = configClass.getItem("ormMysql")
        engine = create_engine(ormMysql['url'], encoding='utf-8', echo=True,pool_recycle=300)
        Base = declarative_base()  # 生成orm基类
        Session = sessionmaker(bind=engine)
        self.session = Session()

    def getSession(self):
        return self.session
