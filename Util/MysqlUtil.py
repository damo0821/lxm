# coding:utf-8

import pymysql
import datetime
import json
import ConfigUtil
from dbutils.pooled_db import PooledDB

config = ConfigUtil.configClass()

class DateEnconding(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.date):
            return o.strftime('%Y-%m-%d %H:%M:%S')

class MysqlClass:

    def __init__(self):
        mysql = config.getItem('mysql')
        POOL = PooledDB(
            creator=pymysql,  # 使用链接数据库的模块
            maxconnections=6,  # 连接池允许的最大连接数，0和None表示不限制连接数
            mincached=2,  # 初始化时，链接池中至少创建的空闲的链接，0表示不创建
            maxcached=5,  # 链接池中最多闲置的链接，0和None不限制
            maxshared=3,  # 链接池中最多共享的链接数量，0和None表示全部共享。PS: 无用，因为pymysql和MySQLdb等模块的 threadsafety都为1，所有值无论设置为多少，_maxcached永远为0，所以永远是所有链接都共享。
            blocking=True,  # 连接池中如果没有可用连接后，是否阻塞等待。True，等待；False，不等待然后报错
            maxusage=None,  # 一个链接最多被重复使用的次数，None表示无限制
            setsession=[],  # 开始会话前执行的命令列表。
            ping=0,
            # ping MySQL服务端，检查是否服务可用。
            host=mysql['mysql_host'],
            port=int(mysql['mysql_port']),
            user=mysql['mysql_user'],
            password=mysql['mysql_password'],
            database=mysql['mysql_database'],
            charset='utf8'
        )
        self.conn = POOL.connection()

    def executeQuery(self, sql):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute(sql)
        return json.dumps(cursor.fetchall(), cls=DateEnconding)

    def executeQueryOne(self, sql):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute(sql)
        return json.dumps(cursor.fetchone(), cls=DateEnconding)

    def executeSave(self, sql):
        cursor = self.conn.cursor()
        cursor.execute(sql)
        self.conn.commit()
