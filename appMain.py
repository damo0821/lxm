# coding:utf-8

from flask import Flask
from flask_apscheduler import APScheduler
from flask import request
import hashlib
import wxMsg.sendWxTemplate as wx
import Util.RedisUtil
import Util.MysqlUtil
import wxMsg.weChatDataProcessing as wxData
import xmltodict
import wxMsg.wxSenMsg as wws
import service.UserService as userService
import json
import service.SmsSenMsgService as smsSenMsgService

scheduler = APScheduler()
app = Flask(__name__)

app.config.from_object('settings')
RedisUtilClass = Util.RedisUtil.RedisUtilClass()
MysqlClass = Util.MysqlUtil.MysqlClass()


# xmltodict.parse():可以将xml数据转为python中的dict字典数据;
# xmltodict.unparse():可以将字典转换为xml字符串



@app.after_request
def cors(environ):
    environ.headers['Access-Control-Allow-Origin'] = '*'
    environ.headers['Access-Control-Allow-Method'] = '*'
    environ.headers['Access-Control-Allow-Headers'] = 'x-requested-with,content-type'
    print(environ.data)
    return environ
