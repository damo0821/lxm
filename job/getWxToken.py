# coding:utf-8
import datetime
import requests
import appMain as app
from ConfigUtil import configClass

configClass = configClass()

redis = app.RedisUtilClass


def getWxTokenFunction():
    wx = configClass.getItem("wx")

    url = "https://api.weixin.qq.com/cgi-bin/token"
    params = {
        "grant_type": "client_credential",
        "appid": wx['appid'],
        "secret": wx['secret']
    }
    r = requests.get(url, params=params)
    json = r.json()
    print(json)
    redis.set('token', json['access_token'])
