#coding:utf-8

import Util.MysqlUtil
import Util.RedisUtil
import appMain as appMain
import service.MdLoadService as mdloadService
from flask import request


app =appMain.app
RedisUtilClass = Util.RedisUtil.RedisUtilClass()
MysqlClass = Util.MysqlUtil.MysqlClass()
@app.route("/queryHomePageData/<id>")
def queryHomePageData(id):
    data = mdloadService.query_id(id)
    print(data)
    return str(data)

@app.route("/updateMd/<id>"  ,methods=['POST', 'GET'])
def updateMd(id):
    json = None
    if request.method == "GET":
        json = request.args.to_dict()
    if request.method == "POST":
        if request.content_type.startswith('application/json'):
            json = request.json
        elif request.content_type.startswith('multipart/form-data'):
            json = request.form
        else:
            json = request.values
    json['id'] = id
    return mdloadService.update(json)
